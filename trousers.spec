Name:         trousers
Version:      0.3.15
Release:      3
Summary:      The open-source TCG Software Stack
License:      BSD
Url:          http://trousers.sourceforge.net
Source0:      https://sourceforge.net/projects/trousers/files/trousers/%{version}/trousers-%{version}.tar.gz
#Acknowledge Source1 from Fedora.
Source1:      tcsd.service

BuildRequires:    make gettext-devel autoconf automake
BuildRequires:    libtool openssl-devel systemd
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

Provides:         trousers-lib
Obsoletes:        trousers-lib < %{version}-%{release}

%description
Trousers is an Trusted Computing Software Stack. By using trousers you
can develop applications based on the Trusted Platform Module(TPM). The
TPM enables more secure storage. It provides hardware-based protection
of data because the private key used to protect the data is never exposed
in the clear outside of the TPM's own internal memory area.

%package devel
Summary: Trousers header files, documentation and trousers tCG device driver library
Requires: %{name} = %{version}-%{release}

Provides:  trousers-static
Obsoletes: trousers-static < %{version}-%{release}

%description devel
Includes header files, static library and other development files using trousers.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1


%build
chmod +x ./bootstrap.sh
./bootstrap.sh
%configure --with-gui=openssl
%make_build -k

%install
mkdir -p %{buildroot}/%{_localstatedir}/lib/tpm
%make_install
%delete_la
mkdir -p %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/

%pre
getent group tss > /dev/null || groupadd -g 59 -r tss
getent passwd tss > /dev/null || \
useradd -r -u 59 -g tss -d /dev/null -s /sbin/nologin \
-c "Account used by the trousers package to sandbox the tcsd daemon" tss
exit 0

%post
%systemd_post tcsd.service
/sbin/ldconfig

%preun
%systemd_preun tcsd.service

%postun
%systemd_postun_with_restart tcsd.service
/sbin/ldconfig

%files
%doc README ChangeLog AUTHORS
%license LICENSE
%{_sbindir}/tcsd
%config(noreplace) %attr(0640, root, tss) %{_sysconfdir}/tcsd.conf
%attr(0644,root,root) %{_unitdir}/tcsd.service
%attr(0700, tss, tss) %{_localstatedir}/lib/tpm/
%{_libdir}/libtspi.so.*

%files devel
%doc doc/LTC-TSS_LLD_08_r2.pdf doc/TSS_programming_SNAFUs.txt
%attr(0755, root, root) %{_libdir}/libtspi.so
%{_includedir}/tss/
%{_includedir}/trousers/
%{_libdir}/libtddl.a

%files help
%{_mandir}/man3/Tspi_*
%{_mandir}/man5/*
%{_mandir}/man8/*


%changelog
* Thu Aug 01 2024 zhangruifang <zhangruifang@h-partners.com> - 0.3.15-3
- The corresponding version number nedds to be added to the Obsoletes filed

* Sat Jan 23 2021 panxiaohe <panxiaohe@huawei.com> - 0.3.15-1
- update to 0.3.15

* Tue Sep 29 2020 Hugel <gengqihu1@huawei.com> - 0.3.14-5
- require /etc/tcsd.conf to be owned by root:tss mode 640 for CVE-2020-24331

* Mon Sep 14 2020 wangchen <wangchen137@huawei.com> - 0.3.14-4
- Fix CVE-2020-24330 CVE-2020-24331 CVE-2020-24332

* Sat Mar 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.3.14-3
- Add tss account used by the trousers package to sandbox the tcsd daemon

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.3.14-2
- Modify requires

* Mon Oct 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.3.14-1
- update to 0.3.14

* Wed Sep 4 2019 Zaiwang Li<lizaiwang1@huawei.com> - 0.3.13-12
- Init package
